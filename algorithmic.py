def descending_array(number) :
	return number != 0 

def findPossibleCombinations(arr, index, num, reducedNum):
	# If zero return nothing
	if (reducedNum < 0): 
		return 
   
	# If reduced number of loop reach 0 actually print
	if (reducedNum == 0):
		array = reversed(list(filter(descending_array, arr)))
		print(",".join(str(number) for number in array))
		return
  
	# Find the previous number stored in arr[].  
	prev = 1 if(index == 0) else arr[index - 1]; 
  
	# loop starts from previous  
	for k in reversed(range(prev, num + 1)): 
		# next element of array is k 
		arr[index] = k; 
		# call recursively
		findPossibleCombinations(arr, index + 1, num,  reducedNum - k); 

def findCombinations(n):  
	# Array to store max (n) element 
	arr = [0] * int(n);
	findPossibleCombinations(arr, 0, int(n), int(n)); 

def factorial_recursive(n):
    # Base case: 1! = 1
    if int(n) == 1:
    	return 1

    # Recursive case: n! = n * (n-1)!
    else:
        return int(n)* factorial_recursive(int(n)-1)

  
# Driver code 
input = input("Masukkan angka : ")
findCombinations(input); 
