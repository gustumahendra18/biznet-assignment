# PYTHON MAP
def primes(iteration):
    # Check if not an integer
    if not isinstance(iteration, int) :
	raise Exception('{value}, is not an Integer'.format(value=iteration)) 
    jumlah_mod = 0
	
    if iteration == 2 :
	return True
	
    for x in range(1, iteration) : 
        if (iteration % x) == 0:
	   jumlah_mod += 1

    if jumlah_mod == 1 :
	return True 

# SET NUMBER HERE 
number = input("Masukkan rentang bilangan : ")
result = filter(primes, range(1, number)) 
print("Number is {}, primes between are : ".format(number), list(result)) 
