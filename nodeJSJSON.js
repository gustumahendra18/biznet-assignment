const { performance } = require('perf_hooks')

const JSONFiles = [{'username':"ali", 'hair_color':'brown', 'height':1.2},{'username':"marc", 'hair_color':'blue', 'height':1.4},{'username':"joe", 'hair_color':'brown', 'height':1.7},{'username':"zehua", 'hair_color':'black', 'height':1.8}]

const JSONImproved = {"h" : ['username', 'hair_color', 'height'], "d" : [['ali', 'brown', 1.2], ['marc', 'blue', 1.4], ['joe', 'brown', 1.7]]}

function improved_JSON(json_data) {
	if(typeof json_data == 'string') {
		try {
			json_data = JSON.parse(json_data);
		} 
		catch(error) { 
			alert (error);
			return;
		}
	}

	var improved_JSON = {};
	var header = new Set();
	const header_values = []

	Object.values(json_data).filter(el => {
	 	const keys = Object.keys(el)
	 	const values = Object.values(el)
	 	header_values.push(values)
	 	keys.map(h => {
	 		const duplicate = header.has(h)
	 		if(!duplicate) 
	 			header.add(h.trim())
	 	})
	})

	header = Array.from(header)
	improved_JSON['h'] = header
	improved_JSON['v'] = header_values

	console.log(json_data)
	const size = Buffer.byteLength(JSON.stringify(json_data))
	console.log(`original size ${size} in bytes`)

	console.log(improved_JSON)
	const size_after = Buffer.byteLength(JSON.stringify(improved_JSON))
	console.log(`improved size ${size_after} in bytes`)
}

// Call Function Here
improved_JSON(JSONFiles)

// // Checking speed
// const t0 = performance.now()
// improved_JSON(JSONFiles)
// const t1 = performance.now()
// console.log("improved_JSON function took " + (t1 - t0) + " milliseconds.")